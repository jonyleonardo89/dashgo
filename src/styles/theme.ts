import { extendTheme } from '@chakra-ui/react';

export const theme = extendTheme({
    color: {
        gray: {
            '900': '#181B23',
            '700': '#2D3748',
            '500': '#616480',
            '300': '#CBD5E0',
            '100': '#EDF2F7',
            '50': '#EEEEF2'
        }
    },

    font: {
        heading: 'Roboto',
        body: 'Roboto'
    },
    styles: {
        global: {
            body: {
                bg: 'gray.900',
                color: 'gray.50'
            }
        }
    }
})

import {Flex, Input as ChakraInput, Button, Stack, FormLabel, InputProps as ChakraInputProps} from '@chakra-ui/react'

interface InputProps extends ChakraInputProps {
    name: string;
    label?: string;

}
//  {...rest pega todas as props do input}
export default function Input({name, label, ...rest}: InputProps) {
    return (
        <div>

            {/*  Só mostra caso o label exista =. !!label && */}
           { !!label &&  <FormLabel htmlFor={name}>{label }</FormLabel>}
            <ChakraInput
                id={name}
                name={name}
                focusBorderColor="pink.500"
                _hover={{
                    bgColor: "gray.900"
                }}
                {...rest}
            />
        </div>
    )
}

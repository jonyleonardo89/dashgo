import { Box, Stack, Text, Link, Icon } from '@chakra-ui/react';
import { RiContactsLine, RiDashboardLine, RiGitRepositoryPrivateLine, RiInputMethodLine } from 'react-icons/ri';
import NavSection from './NavSection';



export default function Sidebar() {
    return (
        <Box as="aside" w="64" mr="8">
            <Stack spacing="12" align="flex-start">
                <Box>
                    <Stack spacing="8">
                        <NavSection titulo="Geral">
                            <Link display="flex" alignItems="center">
                                <Icon as={RiDashboardLine} />
                                <Text ml="4" fontWeight="medium">Dashboard </Text>
                            </Link>
                            <Link display="flex" alignItems="center">
                                <Icon as={RiContactsLine} />
                                <Text ml="4" fontWeight="medium">Usuários </Text>
                            </Link>
                        </NavSection>
                        <NavSection titulo="Automação">
                            <Link display="flex" alignItems="center">
                                <Icon as={RiInputMethodLine} />
                                <Text ml="4" fontWeight="medium">Formulários </Text>
                            </Link>
                            <Link display="flex" alignItems="center">
                                <Icon as={RiGitRepositoryPrivateLine} />
                                <Text ml="4" fontWeight="medium">Automação</Text>
                            </Link>
                        </NavSection>

                    </Stack>
                </Box>
            </Stack >
        </Box>
    )
}


import { Stack, Text, Link, Icon } from '@chakra-ui/react';
import { ReactNode } from 'react';
import { RiContactsLine, RiDashboardLine, RiGitRepositoryPrivateLine, RiInputMethodLine } from 'react-icons/ri';

type NavSectionProps = {
    titulo: string;
    children: ReactNode;
}

export default function NavSection({titulo, children}: NavSectionProps) {
    return (
        <>
            <Text fontWeight="bold" color="gray.400" fontSize="small">
                {titulo}
            </Text>
            <Stack spacing="4" mt="8" align="stretch">
               {children}
            </Stack>
        </>
    )
}

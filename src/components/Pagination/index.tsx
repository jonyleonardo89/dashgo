
import { Stack, Button } from '@chakra-ui/react';

export default function Pagination() {
    return (
        <Stack
            direction="row"
            mt="8"
            justify="space-between"

        >
            <Stack direction="row" spacing="2" >
                <Button
                    size="small"
                    fontSize="xs"
                    width="4"
                    colorScheme="pink"
                    disabled
                    _disabled={{
                        bgColor: 'pink.500',
                        cursor: 'default'
                    }}
                >
                    1
                </Button>
                <Button
                    size="small"
                    fontSize="xs"
                    width="4"
                    colorScheme="gray.700"
                    _hover={{
                        bg: 'gray.500'
                    }}

                >
                    2
                </Button>
                <Button
                    size="small"
                    fontSize="xs"
                    width="4"
                    colorScheme="gray.700"
                    _hover={{
                        bg: 'gray.500'
                    }}

                >
                    3
                </Button>
            </Stack>
        </Stack>
    )
}

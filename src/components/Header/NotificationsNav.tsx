
import { HStack, Icon } from '@chakra-ui/react';
import { RiSearchLine, RiNotificationLine, RiUserAddLine } from "react-icons/ri";


const NotificationNav = () => {
    return (
        <HStack
            spacing="8"
            mx="8"
            pr="8"
            py="1"
            color="gray.300"
            borderColor="gray.700"
        >
        <Icon  as={RiNotificationLine} fontSize="20" mr="8"/>
        <Icon as={RiUserAddLine} fontSize="20"/>

    </HStack>
    );
}

export default NotificationNav;


import { Text } from '@chakra-ui/react';

const Logo = () => {
    return (
        <Text
            fontSize="3xl"
            fontWeight="bold"
            w="64"
        > Dashgo
            <Text as="span" color="pink.500" fontSize="3xl"
                fontWeight="bold">.</Text>
        </Text>
    );
}

export default Logo;

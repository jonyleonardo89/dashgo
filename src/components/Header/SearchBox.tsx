
import { Flex, Icon, Input } from '@chakra-ui/react';
import { RiSearchLine, RiNotificationLine, RiUserAddLine } from "react-icons/ri";

const SearchBox = () => {
    return (
        <Flex as="label"
            flex="1"
            py="4"
            px="8"
            ml="6"
            maxWidth={400}
            alignSelf="center"
            color="gray.200"
            position="relative"
            bg="gray.800"
            borderRadius="full"
        >
                <Input
                    color="gray.50"
                    variant="unstyled"
                    placeholder="Buscar"
                />
                <Icon as={RiSearchLine} />
            </Flex>
     );
}

export default SearchBox;

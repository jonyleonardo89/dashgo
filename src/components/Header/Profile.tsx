import { Flex, Box, Text, Avatar } from '@chakra-ui/react';

export default function Profile() {
    return (
        <Flex align="center" ml="12">
            <Box>
                <Text> Joao Leonardo</Text>
                <Text color="gray.500" fontSize="small">jony.leonardo@teste.com</Text>
            </Box>

            <Avatar size="md" name="João Leonardo" ml="4"/>
        </Flex>
    )
}

import type { NextPage } from 'next'
import {Flex, Input, Button, Stack, FormLabel, Text} from '@chakra-ui/react'
import InputComponent from '../components/Form/Input';
import { SubmitHandler, useForm } from 'react-hook-form'
import * as yup from 'yup'
import {yupResolver} from '@hookform/resolvers/yup'

type SignInFormData = {
    email: string;
    password: string;
}

const schema = yup.object({
    email: yup.string().required('Email obrigatório').email(),
    password: yup.string().required('Senha obrigatória')
})

const Home: NextPage = () => {

    const { register, handleSubmit, formState:  { errors }  } = useForm({
        resolver: yupResolver(schema)
    });

    const handleSignIn: SubmitHandler<SignInFormData> = (values, event) => {
        console.log(values)

        // formState.
    }

    return (
        <Flex
            w="100vw"
            h="100vh"
            alignItems="center"
            justifyContent="center"
        >
            <Flex
                as="form"
                w="100%"
                maxWidth={360}
                bg="gray.700"
                p="8"
                borderRadius="8px"
                flexDir="column"
                onSubmit={handleSubmit(handleSignIn)}
            >
                <Stack spacing="4">
                    <Input
                        {...register('email')}
                    />
                    <Text as="p"
                        fontSize="xs"
                        color="red.500"
                    >{errors.email?.message}</Text>
                    <Input
                        type="password"
                        {...register('password')}
                    />
                    <Text as="p"
                        fontSize="xs"
                        color="red.500"
                    >{errors.password?.message}</Text>
                </Stack>
                <Button type="submit"  mt="6" colorScheme="pink" > Entrar</Button>
            </Flex>
        </Flex>

    )
}

export default Home

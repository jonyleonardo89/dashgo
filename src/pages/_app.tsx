import { AppProps } from 'next/app'
import { ChakraProvider } from '@chakra-ui/react'
import { theme } from '../styles/theme';
import makeServer from '../services/index';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query'

const queryClient = new QueryClient()

//VERIFICA SE O AMBIENTE É DE DEV
if (process.env.NODE_ENV === 'development') {
    //INICIALIZA O SERVIDOR DO MIRAGE-JS
    makeServer();
}

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <ChakraProvider theme={theme}>
            <QueryClientProvider client={queryClient}>
                <Component {...pageProps} />
            </QueryClientProvider>
        </ChakraProvider>

    )
}

export default MyApp;

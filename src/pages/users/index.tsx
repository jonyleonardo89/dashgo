
import { Box, Flex, Heading, Button, Icon, Table, Checkbox, Text, Thead, Tbody, Td, Tr, Th , Spinner} from '@chakra-ui/react';
import { RiAddLine, RiPencilLine } from 'react-icons/ri';
import Header from '../../components/Header';
import Pagination from '../../components/Pagination';
import Sidebar from '../../components/SideBar';
import axios from 'axios';
import { useEffect } from 'react';
import { useQuery } from 'react-query';

export default function UserList() {

    const { data, isLoading, error } = useQuery('users', async () => {
        const data = await axios.get("http://localhost:3000/api/users");
        console.log(data.data.users)
        return data;
    });

    // console.log("query", query)


    useEffect(() => {
    },[])

    return (
        <Box>
            <Header/>

            <Flex
                w="100%"
                my="6"
                maxWidth={1480}
                mx="auto"
                px="6"
            >
                <Sidebar/>
                <Box flex="1" borderRadius={0} bg="gray.700" p="8">
                    <Flex mb="8" justify="space-between" align="center">
                        <Heading size="lg" fontWeight="normal">
                            Usuários
                        </Heading>
                        <Button as="a" size="small" fontSize="sm" colorScheme="pink"  py="1" px="3" leftIcon={<Icon as={RiAddLine}/>} >Criar Novo</Button>
                    </Flex>

                    {
                        isLoading ? (
                            <Flex justify="center">
                                <Spinner/>
                            </Flex>
                        ) : (
                        <>
                            <Table colorScheme="whiteAlpha">
                                <Thead>
                                    <Tr>
                                        <Th px="6" color="gray.300" w="8">
                                            <Checkbox colorScheme="pink"/>
                                        </Th>
                                        <Th> Usuários</Th>
                                        <Th> Data de Cadastro</Th>
                                        <Th w="8"></Th>
                                    </Tr>
                                </Thead>
                                <Tbody>
                                {data?.data.users.map((user: any) => (
                                    <Tr>
                                        <Td px="6">
                                            <Checkbox colorScheme="pink"/>
                                        </Td>

                                        <Td>
                                            <Box>
                                                <Text fontWeight="bold"> {user.name}</Text>
                                                <Text fontSize="sm" color="gray.300">{user.email}</Text>
                                            </Box>
                                        </Td>
                                        <Td>
                                            {user.created_at}
                                        </Td>
                                        <Td>
                                        <Button as="a" size="small" fontSize="sm" colorScheme="purple"  py="1" px="3" leftIcon={<Icon as={RiPencilLine}/>} >Editar</Button>
                                        </Td>
                                    </Tr>
                                 ))}
                                </Tbody>
                            </Table>
                            <Pagination/>
                        </>
                    )

                    }
                </Box>
            </Flex>
        </Box>
    );
}

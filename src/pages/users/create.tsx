
import { Box, Flex, Heading, VStack, HStack, Divider, Button, Icon, Table, Checkbox, Text, Thead, Tbody, Td, Tr, Th, SimpleGrid } from '@chakra-ui/react';
import { RiAddLine, RiPencilLine } from 'react-icons/ri';
import Header from '../../components/Header';
import Pagination from '../../components/Pagination';
import Sidebar from '../../components/SideBar';
import Input from '../../components/Form/Input';


export default function UserList() {

    return (
        <Box>
            <Header/>

            <Flex
                w="100%"
                my="6"
                maxWidth={1480}
                mx="auto"
                px="6"
            >
                <Sidebar/>
                <Box flex="1" borderRadius={0} bg="gray.700" p="8">
                    <Heading size="lg" fontWeight="normal">
                        Criar usuário
                    </Heading>
                    <Divider my="6" borderColor="gray.700" />

                    <VStack spacing="8">
                        <SimpleGrid minChildWidth="240px" spacing="8" w="100%">
                            <Input name="nome" label="Nome" />
                            <Input name="email" label="Email" type="email" />
                            <Input name="password" label="Senha" type="password" />
                            <Input name="password_confirmation" label="Confrimação Senha" type="password" />
                        </SimpleGrid>
                    </VStack>

                    <Flex mt="8" justify="flex-end">
                        <HStack>
                            <Button colorScheme="whiteAlpha">
                                Cancelar
                            </Button>
                            <Button colorScheme="pink">
                                Salvar
                            </Button>
                        </HStack>

                    </Flex>
                </Box>
            </Flex>
        </Box>
    );
}

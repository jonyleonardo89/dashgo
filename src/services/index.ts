import { createServer, Model, Factory } from 'miragejs';
import { faker } from '@faker-js/faker'; // LIB Q PRODUZ DADOS FAKE

type User = {
    name: string;
    email: string;
    creted_at: string;
}

export default function makeServer() {

    //CRIA O SERVIDOR DO MIRAGE
    const server = createServer({
        // SÃO OS DADOS QUE QRO ARMAZENAR NO DB DO MIRAGE
        models: {
            user: Model.extend<Partial<User>>({})
            // <Partial<User>>: HACK DO TS P/ QUANDO NÃO INFORMO TDS OS CAMPOS
        },

        //SERVE PARA GERAR VARIOS DADOS
        factories: {
            user: Factory.extend({
                name(index) {
                    return faker.name.firstName() + " " + faker.name.lastName();
                },
                email() {
                    return faker.internet.email().toLowerCase();
                },
                created_at() {
                    return faker.date.recent(10) // Ultimos 10 dias apartir da data atual
                }
            })
        },

        //SERVE PARA GERAR DADOS DE TESTE
        seeds(server) {
            //cria a lista baseada na factory, com 20 usuarios ( 2 parametro)
            server.createList('user', 20)
        },

        routes() {
            //CAMINHO PARA CHAMAR AS ROTAS DO MIRAGEN
            this.namespace = "/api" // rotas serão do tipo 'api/users
            this.urlPrefix = 'http://localhost:3000'
            this.timing = 750; // Tempo das chamadas 750 milisegundos

            this.get('/users');
            this.post('/users')

            // RESETA O NAMESPACE P/ NÃO CONFLITAR COM AS ROTAS DA PAGE "api";
            this.namespace = "";
            // VERIFICA SE A ROTA É DO MIRAGE, SENÃO PASSA PARA ONDE A ROTA ESTA;
            this.passthrough()
        }
    })
}
